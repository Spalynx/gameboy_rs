use crate::cart::mbc_none::*;

mod mbc_none;

pub trait CART {
    fn get(&self, i: u16) -> u8;
    fn set(&mut self, i: u16, val: u8);
}

//Could do a whole thing with an enum, but I'm gonna try not to
// death spiral here... yet...
pub fn new() -> Box<dyn CART> {
    return Box::new(MbcNone::new());
}
