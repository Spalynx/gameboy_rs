use crate::cart::CART;
use crate::utils::KIB;

/// Mapper 0 - No Memory Bank Controller
/// This is just a ROM chip and /maybe/ 1 bank worth of ram.
/// TODO: Figure out if I can just supply 32kb/8kb with no problems
///       or if I need to figure out via the rom header.
pub struct MbcNone {
    rom: [u8; 32 * KIB], //32KB Rom Chip
    ram: [u8; 8 * KIB],  //8KB RAM - _Potentially_ 1 bank worth of ram.
}

impl MbcNone {
    pub fn new() -> Self {
        Self {
            rom: [0; 32 * KIB],
            ram: [0; 8 * KIB],
        }
    }
}

impl CART for MbcNone {
    fn get(&self, i: u16) -> u8 {
        return match i {
            0x0000..=0x7FFF => self.rom[i as usize],
            0xA000..=0xBFFF => self.ram[(i - 0xA000) as usize],
            _ => panic!("Invalid CART memory index! {}", i),
        };
    }
    fn set(&mut self, i: u16, val: u8) {
        match i {
            0x0000..=0x7FFF => self.rom[i as usize] = val,
            0xA000..=0xBFFF => self.ram[(i - 0xA000) as usize] = val,
            _ => panic!("Invalid CART memory index! {}", i),
        };
    }
}
