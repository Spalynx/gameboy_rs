use js_sys::Atomics::load;

use crate::cpu::instructions::*;
use crate::cpu::regs::*;
use crate::mem::MEM;
use std::cell::RefCell;
use std::rc::Rc;

mod cpu_test;
mod instructions;
mod regs;

pub struct CPU {
    pub pc: u16,
    sp: u16,
    opcode: u16,

    mem: Rc<RefCell<MEM>>,

    a: u8,
    f: u8,
    b: u8,
    c: u8,
    d: u8,
    e: u8,
    h: u8,
    l: u8,

    temp: u16,
}

impl CPU {
    pub fn new(mem_ref: Rc<RefCell<MEM>>) -> Self {
        Self {
            pc: 0,
            sp: 0,
            opcode: 0,
            mem: mem_ref,
            a: 0,
            b: 0,
            d: 0,
            h: 0,
            f: 0,
            c: 0,
            e: 0,
            l: 0,
            temp: 0,
        }
    }

    pub fn new_empty() -> Self {
        let _mem: MEM = MEM::new(false);
        let mem: Rc<RefCell<MEM>> = Rc::new(RefCell::new(_mem));
        Self::new(mem)
    }

    pub fn step(&mut self) {
        let opcode = self.get_mem(self.pc);

        let current = OPS[opcode as usize];
        let params = current.params;

        println!("{}", current);
        match current.name {
            "LD" => self.ld(params[0], params[1]),
            "ADD" => self.add(params[0], params[1]),
            "SUB" => self.sub(params[0], params[1]),
            _ => panic!("Opcode unimplemented!"),
        }

        self.pc += current.cycles as u16;
    }
}
