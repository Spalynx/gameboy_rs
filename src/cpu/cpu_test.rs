#[cfg(test)]
use super::*;
use crate::cpu::regs::*;
use crate::cpu::*;

#[test]
fn it_works() {
    let result = 2 + 2;
    assert_eq!(result, 4);
}

#[test]
fn cpu_init() {
    let cpu = super::CPU::new_empty();
    assert_eq!(cpu.opcode, 0);
    assert_eq!(cpu.pc, 0);
}

#[test]
fn set_flags() {
    let mut cpu = super::CPU::new_empty();

    cpu.f = 0;

    cpu.set_flag(Flag::Z, true);
    assert!(cpu.get_flag(Flag::Z), "Flag Z wasn't set. 0b{:8b}", cpu.f);
    cpu.set_flag(Flag::N, true);
    assert!(cpu.get_flag(Flag::N), "Flag N wasn't set. 0b{:8b}", cpu.f);
    cpu.set_flag(Flag::H, true);
    assert!(cpu.get_flag(Flag::H), "Flag H wasn't set. 0b{:8b}", cpu.f);
    cpu.set_flag(Flag::C, true);
    assert!(cpu.get_flag(Flag::C), "Flag C wasn't set. 0b{:8b}", cpu.f);

    assert_eq!(cpu.f, 0b11110000, "All flags are set.");
}

#[test]
fn unset_flags() {
    let mut cpu = super::CPU::new_empty();

    cpu.f = 0b11111111;

    cpu.set_flag(Flag::Z, false);
    assert!(!cpu.get_flag(Flag::Z), "Flag Z wasnt unset. 0b{:8b}", cpu.f);
    cpu.set_flag(Flag::N, false);
    assert!(!cpu.get_flag(Flag::N), "Flag N wasnt unset. 0b{:8b}", cpu.f);
    cpu.set_flag(Flag::H, false);
    assert!(!cpu.get_flag(Flag::H), "Flag H wasnt unset. 0b{:8b}", cpu.f);
    cpu.set_flag(Flag::C, false);
    assert!(!cpu.get_flag(Flag::C), "Flag C wasnt unset. 0b{:8b}", cpu.f);

    assert_eq!(cpu.f, 0b00001111, "All flags are unset.");
}

#[test]
//I decided not to have an OPNUM var in the Op object,
// at the risk of accidentally backspacing the list into
// a failing emulator. This should help.
fn ensure_opcode_list_is_correct() {
    assert_eq!(OPS[0].name, "NOP");
    assert_eq!(OPS[10].name, "LD");
    assert_eq!(OPS[25].name, "ADD");
    assert_eq!(OPS[50].name, "LD");
    assert_eq!(OPS[100].name, "LD");
    assert_eq!(OPS[118].name, "HALT");
    assert_eq!(OPS[127].name, "LD");
    assert_eq!(OPS[128].name, "ADD");
    assert_eq!(OPS[144].name, "SUB");
    assert_eq!(OPS[160].name, "AND");
    assert_eq!(OPS[200].name, "RET");
    assert_eq!(OPS[203].name, "PREFIX");
    assert_eq!(OPS[235].name, "ILLEGAL_EB");
    assert_eq!(OPS[255].name, "RST");
}

#[test]
fn op_ld_bc_imm16() {
    let mut cpu = super::CPU::new_empty();

    cpu.set_mem(0, 1); //opcode
    cpu.pc = 0;
    cpu.set_mem(1, 0xff); //from
    cpu.set_mem(2, 0xff); //from
    cpu.b = 0; //into
    cpu.c = 0; //into

    //act
    cpu.step();

    assert_eq!(cpu.c, 0xff, "Cpu.C was not set correctly!");
    assert_eq!(cpu.b, 0xff, "Cpu.b was not set correctly!");
}
#[test]
fn op_ld_indbc_a() {
    let mut cpu = super::CPU::new_empty();

    cpu.pc = 0;
    cpu.set_mem(0, 2); //opcode

    cpu.c = 0;
    cpu.b = 0xff;
    cpu.set_mem(0xff, 1); //into

    cpu.a = 0x22; //from

    //act
    cpu.step();

    assert_eq!(
        cpu.get_mem(CPU::combine(cpu.c, cpu.b)),
        0x22,
        "Mem(bc) was not set correctly to cpu.a"
    );
}
#[test]
fn op_ld_indde_a() {
    let mut cpu = super::CPU::new_empty();

    cpu.pc = 0;
    cpu.set_mem(0, 18); //opcode

    cpu.e = 0;
    cpu.d = 0xff;
    cpu.set_mem(0xff, 1); //into - IndDE

    cpu.a = 0x22; //from - A

    //act
    cpu.step();

    assert_eq!(
        cpu.get_mem(CPU::combine(cpu.e, cpu.d)),
        0x22,
        "Mem(de) was not set correctly to cpu.a"
    );
}
#[test]
fn op_ld_hldec_a() {
    let mut cpu = super::CPU::new_empty();

    cpu.pc = 0;
    cpu.set_mem(0, 50); //opcode

    cpu.l = 0x11;
    cpu.h = 0x22;
    cpu.set_mem(0x1122, 1); //into - HLdec

    cpu.a = 0x22; //from - A

    //act
    cpu.step();

    assert_eq!(
        cpu.get_mem(0x1122),
        0x22,
        "Mem(HL) was not set correctly to cpu.a"
    );
    assert_eq!(cpu.h, 0x21, "HL was not decremented.");
}
