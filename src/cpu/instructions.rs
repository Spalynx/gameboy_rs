use crate::cpu::regs::*;
use crate::cpu::CPU;
use crate::utils::*;
use std::convert::TryInto;
use std::fmt;

impl CPU {
    pub fn add(&mut self, into: Reg, operand: Reg) {
        let into_val = self.get(into.clone());
        let operand_val = self.get(operand);

        let result = into_val.wrapping_add(operand_val);

        //Set flags
        self.set_flag(Flag::Z, result == 0);
        self.set_flag(Flag::N, false);
        self.set_flag(Flag::H, half_carryp(into_val, operand_val));
        //TODO: Figure out carries.
        self.set_flag(Flag::C, carryp(into_val, operand_val));

        self.set(into, result);
    }
    pub fn sub(&mut self, into: Reg, operand: Reg) {
        let result = self.get(into.clone()).wrapping_add(self.get(operand));
        self.set(into, result)
    }
    pub fn ld(&mut self, into: Reg, from: Reg) {
        self.set(into, self.get(from));
    }
    pub fn inc(&mut self, into: Reg, from: Reg) {
        self.set(into, self.get(from));
    }
    pub fn dec(&mut self, into: Reg, from: Reg) {
        self.set(into, self.get(from));
    }
    pub fn nop(&mut self) {
        unimplemented!();
        //op!("NOP", 1, 4, [Reg::NONE, Reg::NONE]),
    }
    pub fn and(&mut self) {
        unimplemented!();
        //op!("AND", 2, 8, [Reg::A, Reg::Imm8]),
    }
    pub fn or(&mut self) {
        unimplemented!();
        //op!("OR", 1, 4, [Reg::A, Reg::B]),
    }
    pub fn xor(&mut self) {
        unimplemented!();
        //op!("XOR", 1, 4, [Reg::A, Reg::B]),
    }
    pub fn ldh(&mut self) {
        unimplemented!();
        //op!("LDH", 2, 12, [Reg::IndImm8, Reg::A]),
    }
    pub fn cp(&mut self) {
        unimplemented!();
        //op!("CP", 1, 4, [Reg::A, Reg::A]),
    }
    pub fn adc(&mut self) {
        unimplemented!();
        //op!("ADC", 1, 4, [Reg::A, Reg::B]),
    }
    pub fn sbc(&mut self) {
        unimplemented!();
        //op!("SBC", 1, 4, [Reg::A, Reg::B]),
    }
    pub fn halt(&mut self) {
        unimplemented!();
        //op!("HALT", 1, 4, [Reg::NONE, Reg::NONE]),
    }
    pub fn stop(&mut self) {
        unimplemented!();
        //op!("STOP", 2, 4, [Reg::Imm8, Reg::NONE]),
    }

    pub fn jr(&mut self) {
        unimplemented!();
        //op!("JR", 2, 12, [Reg::Imm8, Reg::NONE]),
    }
    pub fn jp(&mut self) {
        unimplemented!();
        //op!("JP", 3, 12, [Reg::FlagZNot, Reg::Imm16]),
    }
    pub fn rst(&mut self) {
        unimplemented!();
        //op!("RST", 1, 16, [Reg::NONE, Reg::NONE]),
    }
    pub fn ret(&mut self) {
        unimplemented!();
        //op!("RET", 1, 8, [Reg::FlagCNot, Reg::NONE]),
    }
    pub fn reti(&mut self) {
        unimplemented!();
        //op!("RETI", 1, 16, [Reg::NONE, Reg::NONE]),
    }
    pub fn call(&mut self) {
        unimplemented!();
        //op!("CALL", 3, 12, [Reg::FlagZNot, Reg::Imm16]),
    }
    pub fn push(&mut self) {
        unimplemented!();
        //op!("PUSH", 1, 16, [Reg::BC, Reg::NONE]),
    }
    pub fn pop(&mut self) {
        unimplemented!();
        //op!("POP", 1, 12, [Reg::BC, Reg::NONE]),
    }
    pub fn illegal_d3(&mut self) {
        unimplemented!();
        //op!("ILLEGAL_D3", 1, 4, [Reg::NONE, Reg::NONE]),
    }
    pub fn illegal_db(&mut self) {
        unimplemented!();
        //op!("ILLEGAL_DB", 1, 4, [Reg::NONE, Reg::NONE]),
    }
    pub fn illegal_dd(&mut self) {
        unimplemented!();
        //op!("ILLEGAL_DD", 1, 4, [Reg::NONE, Reg::NONE]),
    }
    pub fn illegal_e3(&mut self) {
        unimplemented!();
        //op!("ILLEGAL_E3", 1, 4, [Reg::NONE, Reg::NONE]),
    }
    pub fn illegal_e4(&mut self) {
        unimplemented!();
        //op!("ILLEGAL_E4", 1, 4, [Reg::NONE, Reg::NONE]),
    }
    pub fn illegal_eb(&mut self) {
        unimplemented!();
        //op!("ILLEGAL_EB", 1, 4, [Reg::NONE, Reg::NONE]),
    }
    pub fn illegal_ec(&mut self) {
        unimplemented!();
        //op!("ILLEGAL_EC", 1, 4, [Reg::NONE, Reg::NONE]),
    }
    pub fn illegal_ed(&mut self) {
        unimplemented!();
        //op!("ILLEGAL_ED", 1, 4, [Reg::NONE, Reg::NONE]),
    }
    pub fn illegal_f4(&mut self) {
        unimplemented!();
        //op!("ILLEGAL_F4", 1, 4, [Reg::NONE, Reg::NONE]),
    }
    pub fn illegal_fc(&mut self) {
        unimplemented!();
        //op!("ILLEGAL_FC", 1, 4, [Reg::NONE, Reg::NONE]),
    }
    pub fn illegal_fd(&mut self) {
        unimplemented!();
        //op!("ILLEGAL_FD", 1, 4, [Reg::NONE, Reg::NONE]),
    }
    pub fn prefix(&mut self) {
        unimplemented!();
        //op!("PREFIX", 1, 4, [Reg::NONE, Reg::NONE]),
    }
    pub fn rlca(&mut self) {
        unimplemented!();
        //op!("RLCA", 1, 4, [Reg::NONE, Reg::NONE]),
    }
    pub fn rrca(&mut self) {
        unimplemented!();
        //op!("RRCA", 1, 4, [Reg::NONE, Reg::NONE]),
    }
    pub fn rla(&mut self) {
        unimplemented!();
        //op!("RLA", 1, 4, [Reg::NONE, Reg::NONE]),
    }
    pub fn rra(&mut self) {
        unimplemented!();
        //op!("RRA", 1, 4, [Reg::NONE, Reg::NONE]),
    }
    pub fn daa(&mut self) {
        unimplemented!();
        //op!("DAA", 1, 4, [Reg::NONE, Reg::NONE]),
    }
    pub fn cpl(&mut self) {
        unimplemented!();
        //op!("CPL", 1, 4, [Reg::NONE, Reg::NONE]),
    }
    pub fn scf(&mut self) {
        unimplemented!();
        //op!("SCF", 1, 4, [Reg::NONE, Reg::NONE]),
    }
    pub fn ccf(&mut self) {
        unimplemented!();
        //op!("CCF", 1, 4, [Reg::NONE, Reg::NONE]),
    }
    pub fn di(&mut self) {
        unimplemented!();
        //op!("DI", 1, 4, [Reg::NONE, Reg::NONE]),
    }
    pub fn ei(&mut self) {
        unimplemented!();
        //op!("EI", 1, 4, [Reg::NONE, Reg::NONE]),
    }
}

#[derive(Clone, Copy)]
pub struct Op<'a> {
    pub name: &'a str,
    pub length: u8,
    pub cycles: u8,
    pub params: [Reg; 2],
}

impl<'a> fmt::Display for Op<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let p1 = self.params[0];
        let p2 = self.params[1];
        write!(
            f,
            "{}({:?}, {:?}) - Cycles: {}, Length: {}",
            self.name, p1, p2, self.cycles, self.length
        )
    }
}

macro_rules! op {
    ($name:expr, $length:expr, $cycles:expr, $params:expr ) => {
        Op {
            name: $name,
            length: $length,
            cycles: $cycles,
            params: $params,
        }
    };
}

pub static OPS: [Op; 256] = [
    op!("NOP", 1, 4, [Reg::NONE, Reg::NONE]),
    op!("LD", 3, 12, [Reg::BC, Reg::Imm16]),
    op!("LD", 1, 8, [Reg::IndBC, Reg::A]),
    op!("INC", 1, 8, [Reg::BC, Reg::NONE]),
    op!("INC", 1, 4, [Reg::B, Reg::NONE]),
    op!("DEC", 1, 4, [Reg::B, Reg::NONE]),
    op!("LD", 2, 8, [Reg::B, Reg::Imm8]),
    op!("RLCA", 1, 4, [Reg::NONE, Reg::NONE]),
    op!("LD", 3, 20, [Reg::IndImm16, Reg::SP]),
    op!("ADD", 1, 8, [Reg::HL, Reg::BC]),
    op!("LD", 1, 8, [Reg::A, Reg::IndBC]),
    op!("DEC", 1, 8, [Reg::BC, Reg::NONE]),
    op!("INC", 1, 4, [Reg::C, Reg::NONE]),
    op!("DEC", 1, 4, [Reg::C, Reg::NONE]),
    op!("LD", 2, 8, [Reg::C, Reg::Imm8]),
    op!("RRCA", 1, 4, [Reg::NONE, Reg::NONE]),
    op!("STOP", 2, 4, [Reg::Imm8, Reg::NONE]),
    op!("LD", 3, 12, [Reg::DE, Reg::Imm16]),
    op!("LD", 1, 8, [Reg::IndDE, Reg::A]),
    op!("INC", 1, 8, [Reg::DE, Reg::NONE]),
    op!("INC", 1, 4, [Reg::D, Reg::NONE]),
    op!("DEC", 1, 4, [Reg::D, Reg::NONE]),
    op!("LD", 2, 8, [Reg::D, Reg::Imm8]),
    op!("RLA", 1, 4, [Reg::NONE, Reg::NONE]),
    op!("JR", 2, 12, [Reg::Imm8, Reg::NONE]),
    op!("ADD", 1, 8, [Reg::HL, Reg::DE]),
    op!("LD", 1, 8, [Reg::A, Reg::IndDE]),
    op!("DEC", 1, 8, [Reg::DE, Reg::NONE]),
    op!("INC", 1, 4, [Reg::E, Reg::NONE]),
    op!("DEC", 1, 4, [Reg::E, Reg::NONE]),
    op!("LD", 2, 8, [Reg::E, Reg::Imm8]),
    op!("RRA", 1, 4, [Reg::NONE, Reg::NONE]),
    op!("JR", 2, 8, [Reg::FlagZNot, Reg::Imm8]),
    op!("LD", 3, 12, [Reg::HL, Reg::Imm16]),
    op!("LD", 1, 8, [Reg::HLinc, Reg::A]),
    op!("INC", 1, 8, [Reg::HL, Reg::NONE]),
    op!("INC", 1, 4, [Reg::H, Reg::NONE]),
    op!("DEC", 1, 4, [Reg::H, Reg::NONE]),
    op!("LD", 2, 8, [Reg::H, Reg::Imm8]),
    op!("DAA", 1, 4, [Reg::NONE, Reg::NONE]),
    op!("JR", 2, 8, [Reg::FlagZ, Reg::Imm8]),
    op!("ADD", 1, 8, [Reg::HL, Reg::HL]),
    op!("LD", 1, 8, [Reg::A, Reg::HLinc]),
    op!("DEC", 1, 8, [Reg::HL, Reg::NONE]),
    op!("INC", 1, 4, [Reg::L, Reg::NONE]),
    op!("DEC", 1, 4, [Reg::L, Reg::NONE]),
    op!("LD", 2, 8, [Reg::L, Reg::Imm8]),
    op!("CPL", 1, 4, [Reg::NONE, Reg::NONE]),
    op!("JR", 2, 8, [Reg::FlagCNot, Reg::Imm8]),
    op!("LD", 3, 12, [Reg::SP, Reg::Imm16]),
    op!("LD", 1, 8, [Reg::HLdec, Reg::A]),
    op!("INC", 1, 8, [Reg::SP, Reg::NONE]),
    op!("INC", 1, 12, [Reg::IndHL, Reg::NONE]),
    op!("DEC", 1, 12, [Reg::IndHL, Reg::NONE]),
    op!("LD", 2, 12, [Reg::IndHL, Reg::Imm8]),
    op!("SCF", 1, 4, [Reg::NONE, Reg::NONE]),
    op!("JR", 2, 8, [Reg::FlagC, Reg::Imm8]),
    op!("ADD", 1, 8, [Reg::HL, Reg::SP]),
    op!("LD", 1, 8, [Reg::A, Reg::HLdec]),
    op!("DEC", 1, 8, [Reg::SP, Reg::NONE]),
    op!("INC", 1, 4, [Reg::A, Reg::NONE]),
    op!("DEC", 1, 4, [Reg::A, Reg::NONE]),
    op!("LD", 2, 8, [Reg::A, Reg::Imm8]),
    op!("CCF", 1, 4, [Reg::NONE, Reg::NONE]),
    op!("LD", 1, 4, [Reg::B, Reg::B]),
    op!("LD", 1, 4, [Reg::B, Reg::C]),
    op!("LD", 1, 4, [Reg::B, Reg::D]),
    op!("LD", 1, 4, [Reg::B, Reg::E]),
    op!("LD", 1, 4, [Reg::B, Reg::H]),
    op!("LD", 1, 4, [Reg::B, Reg::L]),
    op!("LD", 1, 8, [Reg::B, Reg::IndHL]),
    op!("LD", 1, 4, [Reg::B, Reg::A]),
    op!("LD", 1, 4, [Reg::C, Reg::B]),
    op!("LD", 1, 4, [Reg::C, Reg::C]),
    op!("LD", 1, 4, [Reg::C, Reg::D]),
    op!("LD", 1, 4, [Reg::C, Reg::E]),
    op!("LD", 1, 4, [Reg::C, Reg::H]),
    op!("LD", 1, 4, [Reg::C, Reg::L]),
    op!("LD", 1, 8, [Reg::C, Reg::IndHL]),
    op!("LD", 1, 4, [Reg::C, Reg::A]),
    op!("LD", 1, 4, [Reg::D, Reg::B]),
    op!("LD", 1, 4, [Reg::D, Reg::C]),
    op!("LD", 1, 4, [Reg::D, Reg::D]),
    op!("LD", 1, 4, [Reg::D, Reg::E]),
    op!("LD", 1, 4, [Reg::D, Reg::H]),
    op!("LD", 1, 4, [Reg::D, Reg::L]),
    op!("LD", 1, 8, [Reg::D, Reg::IndHL]),
    op!("LD", 1, 4, [Reg::D, Reg::A]),
    op!("LD", 1, 4, [Reg::E, Reg::B]),
    op!("LD", 1, 4, [Reg::E, Reg::C]),
    op!("LD", 1, 4, [Reg::E, Reg::D]),
    op!("LD", 1, 4, [Reg::E, Reg::E]),
    op!("LD", 1, 4, [Reg::E, Reg::H]),
    op!("LD", 1, 4, [Reg::E, Reg::L]),
    op!("LD", 1, 8, [Reg::E, Reg::IndHL]),
    op!("LD", 1, 4, [Reg::E, Reg::A]),
    op!("LD", 1, 4, [Reg::H, Reg::B]),
    op!("LD", 1, 4, [Reg::H, Reg::C]),
    op!("LD", 1, 4, [Reg::H, Reg::D]),
    op!("LD", 1, 4, [Reg::H, Reg::E]),
    op!("LD", 1, 4, [Reg::H, Reg::H]),
    op!("LD", 1, 4, [Reg::H, Reg::L]),
    op!("LD", 1, 8, [Reg::H, Reg::IndHL]),
    op!("LD", 1, 4, [Reg::H, Reg::A]),
    op!("LD", 1, 4, [Reg::L, Reg::B]),
    op!("LD", 1, 4, [Reg::L, Reg::C]),
    op!("LD", 1, 4, [Reg::L, Reg::D]),
    op!("LD", 1, 4, [Reg::L, Reg::E]),
    op!("LD", 1, 4, [Reg::L, Reg::H]),
    op!("LD", 1, 4, [Reg::L, Reg::L]),
    op!("LD", 1, 8, [Reg::L, Reg::IndHL]),
    op!("LD", 1, 4, [Reg::L, Reg::A]),
    op!("LD", 1, 8, [Reg::IndHL, Reg::B]),
    op!("LD", 1, 8, [Reg::IndHL, Reg::C]),
    op!("LD", 1, 8, [Reg::IndHL, Reg::D]),
    op!("LD", 1, 8, [Reg::IndHL, Reg::E]),
    op!("LD", 1, 8, [Reg::IndHL, Reg::H]),
    op!("LD", 1, 8, [Reg::IndHL, Reg::L]),
    op!("HALT", 1, 4, [Reg::NONE, Reg::NONE]),
    op!("LD", 1, 8, [Reg::IndHL, Reg::A]),
    op!("LD", 1, 4, [Reg::A, Reg::B]),
    op!("LD", 1, 4, [Reg::A, Reg::C]),
    op!("LD", 1, 4, [Reg::A, Reg::D]),
    op!("LD", 1, 4, [Reg::A, Reg::E]),
    op!("LD", 1, 4, [Reg::A, Reg::H]),
    op!("LD", 1, 4, [Reg::A, Reg::L]),
    op!("LD", 1, 8, [Reg::A, Reg::IndHL]),
    op!("LD", 1, 4, [Reg::A, Reg::A]),
    op!("ADD", 1, 4, [Reg::A, Reg::B]),
    op!("ADD", 1, 4, [Reg::A, Reg::C]),
    op!("ADD", 1, 4, [Reg::A, Reg::D]),
    op!("ADD", 1, 4, [Reg::A, Reg::E]),
    op!("ADD", 1, 4, [Reg::A, Reg::H]),
    op!("ADD", 1, 4, [Reg::A, Reg::L]),
    op!("ADD", 1, 8, [Reg::A, Reg::IndHL]),
    op!("ADD", 1, 4, [Reg::A, Reg::A]),
    op!("ADC", 1, 4, [Reg::A, Reg::B]),
    op!("ADC", 1, 4, [Reg::A, Reg::C]),
    op!("ADC", 1, 4, [Reg::A, Reg::D]),
    op!("ADC", 1, 4, [Reg::A, Reg::E]),
    op!("ADC", 1, 4, [Reg::A, Reg::H]),
    op!("ADC", 1, 4, [Reg::A, Reg::L]),
    op!("ADC", 1, 8, [Reg::A, Reg::IndHL]),
    op!("ADC", 1, 4, [Reg::A, Reg::A]),
    op!("SUB", 1, 4, [Reg::A, Reg::B]),
    op!("SUB", 1, 4, [Reg::A, Reg::C]),
    op!("SUB", 1, 4, [Reg::A, Reg::D]),
    op!("SUB", 1, 4, [Reg::A, Reg::E]),
    op!("SUB", 1, 4, [Reg::A, Reg::H]),
    op!("SUB", 1, 4, [Reg::A, Reg::L]),
    op!("SUB", 1, 8, [Reg::A, Reg::IndHL]),
    op!("SUB", 1, 4, [Reg::A, Reg::A]),
    op!("SBC", 1, 4, [Reg::A, Reg::B]),
    op!("SBC", 1, 4, [Reg::A, Reg::C]),
    op!("SBC", 1, 4, [Reg::A, Reg::D]),
    op!("SBC", 1, 4, [Reg::A, Reg::E]),
    op!("SBC", 1, 4, [Reg::A, Reg::H]),
    op!("SBC", 1, 4, [Reg::A, Reg::L]),
    op!("SBC", 1, 8, [Reg::A, Reg::IndHL]),
    op!("SBC", 1, 4, [Reg::A, Reg::A]),
    op!("AND", 1, 4, [Reg::A, Reg::B]),
    op!("AND", 1, 4, [Reg::A, Reg::C]),
    op!("AND", 1, 4, [Reg::A, Reg::D]),
    op!("AND", 1, 4, [Reg::A, Reg::E]),
    op!("AND", 1, 4, [Reg::A, Reg::H]),
    op!("AND", 1, 4, [Reg::A, Reg::L]),
    op!("AND", 1, 8, [Reg::A, Reg::IndHL]),
    op!("AND", 1, 4, [Reg::A, Reg::A]),
    op!("XOR", 1, 4, [Reg::A, Reg::B]),
    op!("XOR", 1, 4, [Reg::A, Reg::C]),
    op!("XOR", 1, 4, [Reg::A, Reg::D]),
    op!("XOR", 1, 4, [Reg::A, Reg::E]),
    op!("XOR", 1, 4, [Reg::A, Reg::H]),
    op!("XOR", 1, 4, [Reg::A, Reg::L]),
    op!("XOR", 1, 8, [Reg::A, Reg::IndHL]),
    op!("XOR", 1, 4, [Reg::A, Reg::A]),
    op!("OR", 1, 4, [Reg::A, Reg::B]),
    op!("OR", 1, 4, [Reg::A, Reg::C]),
    op!("OR", 1, 4, [Reg::A, Reg::D]),
    op!("OR", 1, 4, [Reg::A, Reg::E]),
    op!("OR", 1, 4, [Reg::A, Reg::H]),
    op!("OR", 1, 4, [Reg::A, Reg::L]),
    op!("OR", 1, 8, [Reg::A, Reg::IndHL]),
    op!("OR", 1, 4, [Reg::A, Reg::A]),
    op!("CP", 1, 4, [Reg::A, Reg::B]),
    op!("CP", 1, 4, [Reg::A, Reg::C]),
    op!("CP", 1, 4, [Reg::A, Reg::D]),
    op!("CP", 1, 4, [Reg::A, Reg::E]),
    op!("CP", 1, 4, [Reg::A, Reg::H]),
    op!("CP", 1, 4, [Reg::A, Reg::L]),
    op!("CP", 1, 8, [Reg::A, Reg::IndHL]),
    op!("CP", 1, 4, [Reg::A, Reg::A]),
    op!("RET", 1, 8, [Reg::FlagZNot, Reg::NONE]),
    op!("POP", 1, 12, [Reg::BC, Reg::NONE]),
    op!("JP", 3, 12, [Reg::FlagZNot, Reg::Imm16]),
    op!("JP", 3, 16, [Reg::Imm16, Reg::NONE]),
    op!("CALL", 3, 12, [Reg::FlagZNot, Reg::Imm16]),
    op!("PUSH", 1, 16, [Reg::BC, Reg::NONE]),
    op!("ADD", 2, 8, [Reg::A, Reg::Imm8]),
    op!("RST", 1, 16, [Reg::NONE, Reg::NONE]),
    op!("RET", 1, 8, [Reg::FlagZ, Reg::NONE]),
    op!("RET", 1, 16, [Reg::NONE, Reg::NONE]),
    op!("JP", 3, 12, [Reg::FlagZ, Reg::Imm16]),
    op!("PREFIX", 1, 4, [Reg::NONE, Reg::NONE]),
    op!("CALL", 3, 12, [Reg::FlagZ, Reg::Imm16]),
    op!("CALL", 3, 24, [Reg::Imm16, Reg::NONE]),
    op!("ADC", 2, 8, [Reg::A, Reg::Imm8]),
    op!("RST", 1, 16, [Reg::NONE, Reg::NONE]),
    op!("RET", 1, 8, [Reg::FlagCNot, Reg::NONE]),
    op!("POP", 1, 12, [Reg::DE, Reg::NONE]),
    op!("JP", 3, 12, [Reg::FlagCNot, Reg::Imm16]),
    op!("ILLEGAL_D3", 1, 4, [Reg::NONE, Reg::NONE]),
    op!("CALL", 3, 12, [Reg::FlagCNot, Reg::Imm16]),
    op!("PUSH", 1, 16, [Reg::DE, Reg::NONE]),
    op!("SUB", 2, 8, [Reg::A, Reg::Imm8]),
    op!("RST", 1, 16, [Reg::NONE, Reg::NONE]),
    op!("RET", 1, 8, [Reg::FlagC, Reg::NONE]),
    op!("RETI", 1, 16, [Reg::NONE, Reg::NONE]),
    op!("JP", 3, 12, [Reg::FlagC, Reg::Imm16]),
    op!("ILLEGAL_DB", 1, 4, [Reg::NONE, Reg::NONE]),
    op!("CALL", 3, 12, [Reg::FlagC, Reg::Imm16]),
    op!("ILLEGAL_DD", 1, 4, [Reg::NONE, Reg::NONE]),
    op!("SBC", 2, 8, [Reg::A, Reg::Imm8]),
    op!("RST", 1, 16, [Reg::NONE, Reg::NONE]),
    op!("LDH", 2, 12, [Reg::IndImm8, Reg::A]),
    op!("POP", 1, 12, [Reg::HL, Reg::NONE]),
    op!("LDH", 1, 8, [Reg::IndC, Reg::A]),
    op!("ILLEGAL_E3", 1, 4, [Reg::NONE, Reg::NONE]),
    op!("ILLEGAL_E4", 1, 4, [Reg::NONE, Reg::NONE]),
    op!("PUSH", 1, 16, [Reg::HL, Reg::NONE]),
    op!("AND", 2, 8, [Reg::A, Reg::Imm8]),
    op!("RST", 1, 16, [Reg::NONE, Reg::NONE]),
    op!("ADD", 2, 16, [Reg::SP, Reg::Imm8]),
    op!("JP", 1, 4, [Reg::HL, Reg::NONE]),
    op!("LD", 3, 16, [Reg::IndImm16, Reg::A]),
    op!("ILLEGAL_EB", 1, 4, [Reg::NONE, Reg::NONE]),
    op!("ILLEGAL_EC", 1, 4, [Reg::NONE, Reg::NONE]),
    op!("ILLEGAL_ED", 1, 4, [Reg::NONE, Reg::NONE]),
    op!("XOR", 2, 8, [Reg::A, Reg::Imm8]),
    op!("RST", 1, 16, [Reg::NONE, Reg::NONE]),
    op!("LDH", 2, 12, [Reg::A, Reg::IndImm8]),
    op!("POP", 1, 12, [Reg::AF, Reg::NONE]),
    op!("LDH", 1, 8, [Reg::A, Reg::IndC]),
    op!("DI", 1, 4, [Reg::NONE, Reg::NONE]),
    op!("ILLEGAL_F4", 1, 4, [Reg::NONE, Reg::NONE]),
    op!("PUSH", 1, 16, [Reg::AF, Reg::NONE]),
    op!("OR", 2, 8, [Reg::A, Reg::Imm8]),
    op!("RST", 1, 16, [Reg::NONE, Reg::NONE]),
    op!("LD", 2, 12, [Reg::HL, Reg::SP]), //SP+i8
    op!("LD", 1, 8, [Reg::SP, Reg::HL]),
    op!("LD", 3, 16, [Reg::A, Reg::IndImm16]),
    op!("EI", 1, 4, [Reg::NONE, Reg::NONE]),
    op!("ILLEGAL_FC", 1, 4, [Reg::NONE, Reg::NONE]),
    op!("ILLEGAL_FD", 1, 4, [Reg::NONE, Reg::NONE]),
    op!("CP", 2, 8, [Reg::A, Reg::Imm8]),
    op!("RST", 1, 16, [Reg::NONE, Reg::NONE]),
];
