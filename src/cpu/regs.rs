use crate::cpu::CPU;
use std::cell::RefCell;
use std::rc::Rc;

//16 bit regs are in LITTLE ENDIAN
//The names are the opposite of how they actually are!!!
// Ex. given: a = 0xFF, f = 0x00 :: af = 0x00FF.
#[repr(u8)]
#[derive(Clone, Copy, Debug)]
pub enum Reg {
    //n
    B,
    C,
    D,
    E,
    H,
    L,
    A,
    IndC,
    IndHL,
    HLinc,
    HLdec,
    IndBC,
    IndDE,
    IndImm8,
    IndImm16,
    Imm8,

    //nn
    AF,
    BC,
    DE,
    HL,
    SP,
    Imm16,

    //CC
    FlagCNot,
    FlagZNot,
    FlagZ,
    FlagC,

    NONE,
}

pub enum Flag {
    Z = 7,
    N = 6,
    H = 5,
    C = 4,
}

impl CPU {
    pub fn combine(hi: u8, lo: u8) -> u16 {
        (((hi as u16) << 8) + lo as u16)
    }
    pub fn separate(val: u16) -> (u8, u8) {
        ((val >> 8) as u8, (0xFF & val) as u8)
    }

    pub fn set(&mut self, reg: Reg, val: u16) {
        match reg {
            Reg::AF => (self.f, self.a) = Self::separate(val),
            Reg::BC => (self.c, self.b) = Self::separate(val),
            Reg::DE => (self.e, self.d) = Self::separate(val),
            Reg::HL => (self.l, self.h) = Self::separate(val),
            Reg::IndHL => self.set_mem(self.get(Reg::HL), val as u8),
            Reg::B => self.b = val as u8,
            Reg::C => self.c = val as u8,
            Reg::D => self.d = val as u8,
            Reg::E => self.e = val as u8,
            Reg::H => self.h = val as u8,
            Reg::L => self.l = val as u8,
            Reg::A => self.a = val as u8,
            Reg::IndBC => self.set_mem(self.get(Reg::BC), val as u8),
            Reg::IndDE => self.set_mem(self.get(Reg::DE), val as u8),
            Reg::HLinc => {
                let hl = self.get(Reg::HL);
                self.set_mem(hl, val as u8);
                self.set(Reg::HL, hl + 1);
            }
            Reg::HLdec => {
                let hl = self.get(Reg::HL);
                self.set_mem(hl, val as u8);
                self.set(Reg::HL, hl - 1);
            }
            Reg::IndC => {
                self.set_mem(0xFF00 + self.c as u16, val as u8);
            }
            Reg::IndImm8 => {
                let operand = self.get_operands(0).0 as u16;
                self.set_mem(0xFF00 + operand, val as u8);
            }
            Reg::IndImm16 => {
                let operands = self.get_operands(2);
                self.set_mem(Self::combine(operands.1, operands.0), val as u8);
            }
            _ => {}
        };
    }
    pub fn get(&self, reg: Reg) -> u16 {
        //Mind the little endian.
        return match reg {
            Reg::AF => Self::combine(self.f, self.a),
            Reg::BC => Self::combine(self.c, self.b),
            Reg::DE => Self::combine(self.e, self.d),
            Reg::HL => Self::combine(self.l, self.h),
            Reg::IndHL => self.get_mem(self.get(Reg::HL)) as u16,
            Reg::B => self.b as u16,
            Reg::C => self.c as u16,
            Reg::D => self.d as u16,
            Reg::E => self.e as u16,
            Reg::H => self.h as u16,
            Reg::L => self.l as u16,
            Reg::A => self.a as u16,
            Reg::IndBC => self.get_mem(self.get(Reg::BC)) as u16,
            Reg::IndDE => self.get_mem(self.get(Reg::DE)) as u16,
            //For now these are dummy values and you have to increment with the set!
            Reg::HLinc => self.get(Reg::HL),
            Reg::HLdec => self.get(Reg::HL),
            Reg::Imm8 => self.get_operands(0).0 as u16,
            Reg::Imm16 => {
                let operands = self.get_operands(2);
                Self::combine(operands.1, operands.0) as u16 //LE!
            }
            Reg::IndC => self.get_mem(0xFF00 + self.c as u16) as u16,
            Reg::IndImm8 => {
                let operand = self.get_operands(0).0 as u16;
                self.get_mem(0xFF00 + operand) as u16
            }
            Reg::IndImm16 => {
                let operands = self.get_operands(2);
                self.get_mem(Self::combine(operands.1, operands.0)) as u16
            }
            _ => 0,
        };
    }

    //Looks into the future of the program without incrementing.
    //Could be refactored to reduce get_mem if it's a problem.
    pub fn get_operands(&self, amount: u8) -> (u8, u8, u8) {
        return (
            if amount > 0 {
                self.get_mem(self.pc + 1)
            } else {
                0
            },
            if amount > 1 {
                self.get_mem(self.pc + 2)
            } else {
                0
            },
            if amount > 2 {
                self.get_mem(self.pc + 3)
            } else {
                0
            },
        );
    }

    pub fn set_mem(&mut self, i: u16, val: u8) {
        let _mem_rc = Rc::clone(&self.mem);
        let mut mem = _mem_rc.borrow_mut();

        mem.set(i, val);
    }

    pub fn get_mem(&self, i: u16) -> u8 {
        let _mem_rc = Rc::clone(&self.mem);
        let mem = _mem_rc.borrow();

        mem.get(i)
    }
    pub fn get_flag(&self, flag: Flag) -> bool {
        (self.f >> flag as u8) != 0
    }

    pub fn set_flag(&mut self, flag: Flag, val: bool) {
        let operand = 1 << (flag as u8);

        if val {
            self.f |= operand;
        } else {
            self.f &= !operand;
        }
    }
}
