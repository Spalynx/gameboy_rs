use crate::cpu::CPU;
use crate::mem::MEM;
use crate::ppu::PPU;
use js_sys::Uint8Array;
use std::cell::RefCell;
use std::rc::Rc;
use wasm_bindgen::prelude::*;

pub const NUM_PIXELS: usize = crate::DISP_HEIGHT as usize * crate::DISP_WIDTH as usize;

#[wasm_bindgen]
struct Gameboy {
    pub power: bool,
    pub disp_width: u8,
    pub disp_height: u8,
    display: Rc<RefCell<[u8; NUM_PIXELS]>>, //Called gfx in clip8

    mem: Rc<RefCell<MEM>>,
    cpu: Rc<RefCell<CPU>>,
    ppu: Rc<RefCell<PPU>>,
}

#[wasm_bindgen]
impl Gameboy {
    pub fn new() -> Self {
        let mut pixels = [0; NUM_PIXELS];
        for a in (0..pixels.len()).step_by(5) {
            pixels[a] = 1;
        }
        let display: Rc<RefCell<[u8; NUM_PIXELS]>> = Rc::new(RefCell::new(pixels));

        let _mem: MEM = MEM::new(false);
        let mem: Rc<RefCell<MEM>> = Rc::new(RefCell::new(_mem));

        let _cpu: CPU = CPU::new(Rc::clone(&mem));
        let cpu: Rc<RefCell<CPU>> = Rc::new(RefCell::new(_cpu));

        let _ppu: PPU = PPU::new(Rc::clone(&mem));
        let ppu: Rc<RefCell<PPU>> = Rc::new(RefCell::new(_ppu));

        Self {
            power: false,
            display: Rc::clone(&display),
            disp_width: crate::DISP_WIDTH,
            disp_height: crate::DISP_HEIGHT,

            mem: mem,
            cpu: cpu,
            ppu: ppu,
        }
    }

    #[wasm_bindgen(getter)]
    pub fn display(&self) -> Uint8Array {
        let _disp_rc = Rc::clone(&self.display);
        let disp = _disp_rc.borrow_mut();
        return Uint8Array::from(disp.as_slice());
    }
}
