#![allow(unused)] //Delete after 1.0

extern crate wasm_bindgen;

pub mod cart;
pub mod cpu;
pub mod gameboy;
pub mod mem;
pub mod ppu;
mod utils;

use wasm_bindgen::prelude::*;

pub const DISP_HEIGHT: u8 = 160;
pub const DISP_WIDTH: u8 = 144;
//pub const NUM_PIXELS: usize = DISP_HEIGHT as usize * DISP_WIDTH as usize;
#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}
