use crate::cart::*;
use crate::utils::*;

pub struct MEM {
    cart: Box<dyn CART>, //Contains cart rom/ram, depending on the mapper/mbc.
    vram: Vec<u8>,       //8 KiB Video RAM (VRAM)  - Switchable bank 0/1 if CGB Mode
    ram: [u8; 4 * KIB],  //4 KiB Work RAM (WRAM)
    ram_banks: Vec<u8>,  //4 KiB Work RAM (WRAM) - Switchable bank 1–7 if CGB Mode
    oam: [u8; 0xff],     //Object attribute memory (OAM)
    io_regs: [u8; 0x7f], //I/O Registers
    hram: [u8; 0x7f],    //High RAM (HRAM)
    ie: u8,              //Interrupt Enable register (IE)
}

impl MEM {
    pub fn new(cgb_mode: bool) -> Self {
        let cart: Box<dyn CART> = crate::cart::new();

        let vram: Vec<u8>;
        let ram_banks: Vec<u8>;
        if cgb_mode {
            vram = vec![0; 8 * KIB];
            ram_banks = vec![0; 4 * KIB];
        } else {
            vram = vec![0; 2 * 8 * KIB]; // 2 banks
            ram_banks = vec![0; 7 * 4 * KIB]; // 7 banks.
        }

        Self {
            cart: cart,
            vram: vram,
            ram: [0; 4 * KIB],
            ram_banks: ram_banks,
            oam: [0; 0xff],
            io_regs: [0; 0x7f],
            hram: [0; 0x7f],
            ie: 0,
        }
    }

    pub fn get(&self, i: u16) -> u8 {
        let index = i as usize;
        return match i {
            0x0000..=0x7FFF => self.cart.get(i),
            0x8000..=0x9FFF => self.vram[index - 0x8000],
            0xA000..=0xBFFF => self.cart.get(i),
            0xC000..=0xCFFF => self.ram[index - 0xC000],
            0xD000..=0xDFFF => self.ram_banks[index - 0xD000],
            0xFE00..=0xFE9F => self.oam[index - 0xFE00],
            0xFF00..=0xFF7F => self.io_regs[index - 0xFF00],
            0xFF80..=0xFFFE => self.hram[index - 0xFF80],
            0xFFFF => self.ie,

            //Unusable - Mirror of ram/banks
            0xE000..=0xEE00 => self.ram[index - 0xE000],
            0xEE01..=0xFDFF => self.ram_banks[index - 0xEE01],
            //Unusable
            0xFEA0..=0xFEFF => self.ram[index - 0xFEA0],
        };
    }

    pub fn set(&mut self, i: u16, val: u8) {
        let index = i as usize;

        return match i {
            0x0000..=0x7FFF => self.cart.set(i, val),
            0x8000..=0x9FFF => self.vram[index - 0x8000] = val,
            0xA000..=0xBFFF => self.cart.set(i, val),
            0xC000..=0xCFFF => self.ram[index - 0xC000] = val,
            0xD000..=0xDFFF => self.ram_banks[index - 0xD000] = val,
            0xFE00..=0xFE9F => self.oam[index - 0xFE00] = val,
            0xFF00..=0xFF7F => self.io_regs[index - 0xFF00] = val,
            0xFF80..=0xFFFE => self.hram[index - 0xFF80] = val,
            0xFFFF => self.ie = val,

            //Unusable - Mirror of ram/banks
            0xE000..=0xEE00 => self.ram[index - 0xE000] = val,
            0xEE01..=0xFDFF => self.ram_banks[index - 0xEE01] = val,
            //Unusable
            0xFEA0..=0xFEFF => self.ram[index - 0xFEA0] = val,
        };
    }

    /*
    //Shelving this because I realized that passing ownership through this indexing method,
    // while badass, is just fkn overcomplicated and stupid.
    //Keeping this code here because I'd like to combine get/set match statements somehow,
    // but I'm not sure about a good path there, yet.




    ///Given a u16 memory map location, return a ref to the correct value.
    ///This is required because the gameboy memory map is pretty complicated,
    ///and relies on a varying amount of mem that changes per cart.
    fn index(&mut self, i: u16) -> &mut u8 {
        let index: usize = i as usize;

        return match i {
            // 0000-3FFF - 16 KiB ROM bank 00	From cartridge, usually a fixed bank
            0x0000..=0x3FFF => self.ram.get_mut(index),
            // 4000-7FFF - 16 KiB ROM Bank 01–NN	From cartridge, switchable bank via mapper (if any)
            0x4000..=0x7FFF => self.ram.get_mut(0),
            // 8000-9FFF - 8 KiB Video RAM (VRAM)	In CGB mode, switchable bank 0/1
            0x8000..=0x9FFF => self.ram.get_mut(0),
            // A000-BFFF - 8 KiB External RAM	From cartridge, switchable bank if any
            0xA000..=0xBFFF => self.ram.get_mut(0),
            // C000-CFFF - 4 KiB Work RAM (WRAM)
            0xC000..=0xCFFF => self.ram.get_mut(0),
            // D000-DFFF - 4 KiB Work RAM (WRAM)	In CGB mode, switchable bank 1–7
            0xD000..=0xDFFF => self.ram.get_mut(0),
            // E000-FDFF - Echo RAM (mirror of C000–DDFF)
            // Nintendo says use of this area is prohibited.
            0xE000..=0xFDFF => self.ram.get_mut(0),
            // FE00-FE9F - Object attribute memory (OAM)
            0xFE00..=0xFE9F => self.ram.get_mut(0),
            // FEA0-FEFF - Not Usable	Nintendo says use of this area is prohibited.
            0xFEA0..=0xFEFF => self.ram.get_mut(0),
            // FF00-FF7F - I/O Registers
            0xFF00..=0xFF7F => self.ram.get_mut(0),
            // FF80-FFFE - High RAM (HRAM)
            0xFF80..=0xFFFE => self.ram.get_mut(0),
            // FFFF-FFFF - Interrupt Enable register (IE)
            0xFFFF..=0xFFFF => self.ram.get_mut(0),
        }
        .expect("Index is somehow out of bounds"); //God I write the best errors.
    }
    */
}

#[cfg(test)]
mod test_mem {
    #[test]
    fn mem_init() {
        let _mem = super::MEM::new(false);
        assert!(true)
    }
    #[test]
    fn basic_set_get() {
        let mut mem = super::MEM::new(false);

        let i = 100;
        let val = 69;

        mem.set(i, val);

        assert_eq!(
            mem.get(i),
            val,
            "Basic get/set has failed, implying handing a mut aint grate."
        );
    }

    #[test]
    fn fill_random_stuff_up() {
        let mut mem = super::MEM::new(false);

        let val = 69;

        for i in 0..0x3332 {
            mem.set(i * 5, val);

            if mem.get(i * 5) != val {
                assert_eq!(
                    mem.get(i * 5),
                    val,
                    "Value was not set correctly for some reason."
                );
            }
        }
        assert!(true);
    }
}
