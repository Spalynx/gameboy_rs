use crate::mem::MEM;
use std::cell::RefCell;
use std::rc::Rc;

pub struct PPU {
    mem: Rc<RefCell<MEM>>,
}

impl PPU {
    pub fn new(mem_ref: Rc<RefCell<MEM>>) -> Self {
        Self { mem: mem_ref }
    }

    pub fn new_empty() -> Self {
        let _mem: MEM = MEM::new(false);
        let mem: Rc<RefCell<MEM>> = Rc::new(RefCell::new(_mem));

        Self { mem: mem }
    }
}
