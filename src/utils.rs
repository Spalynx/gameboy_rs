pub fn set_panic_hook() {
    // When the `console_error_panic_hook` feature is enabled, we can call the
    // `set_panic_hook` function at least once during initialization, and then
    // we will get better error messages if our code ever panics.
    //
    // For more details see
    // https://github.com/rustwasm/console_error_panic_hook#readme
    #[cfg(feature = "console_error_panic_hook")]
    console_error_panic_hook::set_once();
}

pub const KIB: usize = 1024;

pub fn half_carryp(a: u16, b: u16) -> bool {
    return (((a & 0xF) + (b & 0xF)) & 0x10) == 0x10;
}

pub fn carryp(a: u16, b: u16) -> bool {
    return (((a & 0xF) + (b & 0xF)) & 0x10) == 0x10;
}
