import * as wasm from "gameboy";

const gb = wasm.Gameboy.new();
const width = gb.disp_width;
const height = gb.disp_height;

//Create color picker event handlers and fire them off with defaults
//Defined in html.
//==============================================================================
var primary_color, secondary_color, primary_rgba, secondary_rgba;

const intToRGB = (val) => {
    //Converts an int value (hex code) of a pixel to an rgb(r,g,b,a) value.
    //We're just gonna go ahead and ignore the alpha byte lol, lmao
    return [((val & 0xFF0000) >> 16), ((val & 0xFF00) >> 8), (val & 0xFF),255];
};
document.getElementById("firstColor").onchange = function () {
    primary_color = parseInt(this.value.substring(1), 16);
    primary_rgba = intToRGB(primary_color);
}
document.getElementById("secondColor").onchange = function () {
    secondary_color = parseInt(this.value.substring(1), 16);
    secondary_rgba = intToRGB(secondary_color);
}

document.getElementById("firstColor").onchange(); 
document.getElementById("secondColor").onchange(); 

//Draw screen to a hidden canvas,
//Then copy it to an <img> for scaling and such.
//==============================================================================
var screen = document.getElementById("screen");
screen.imageSmoothingEnabled = false;
screen.mozImageSmoothingEnabled = false;
screen.webkitImageSmoothingEnabled = false;
screen.msImageSmoothingEnabled = false;

var screenCanvas = document.querySelector("#screenBuffer");
screenCanvas.width = width;
screenCanvas.height = height;
var canvasCtx = screenCanvas.getContext("2d");

//var primary_color = Number("0x" + document.getElementById("firstColor").value.substring(1));
//Draw the rust side display as an image.
const drawDisplay = () => {
    //This is slow, figure out just making this Uint32Array -> Uint8ClampedArray with flatmap.
    //let rgbaDisplay = Array.from(display).flatMap((pix) => intToRGB(pix));
    let rgbaDisplay = Array.from(gb.display).flatMap((pix) =>
        pix == 0 ? secondary_rgba : primary_rgba
    );
    let imgData = new ImageData(new Uint8ClampedArray(rgbaDisplay), width);
    canvasCtx.putImageData(imgData, 0, 0);

    screen.src = screenCanvas.toDataURL("image/png");
}





//FINALLY -- Run the chip.
//==============================================================================

// create Oscillator node
function playBeep() {
    var audioCtx = new(window.AudioContext || window.webkitAudioContext)();
    var oscillator = audioCtx.createOscillator();

    oscillator.type = 'sine';
    oscillator.frequency.value = 600; // value in hertz
    oscillator.connect(audioCtx.destination);

    oscillator.start();
    oscillator.stop(0.05);
}
//Main render loop, just draws the display every cycle.
//Not sure if I'm gonna sync display to the emulator or just crank it.
const renderLoop = () => {
    drawDisplay();
    setTimeout(renderLoop, 16.666666666667);
};

var modal = document.getElementById("theModal");
var btn = document.getElementById("btn");
var span = document.getElementsByClassName("close")[0];

btn.onclick = function(){
    modal.style.display = "block";
}

span.onclick = function(){
    modal.style.display = "none";
}

renderLoop();
